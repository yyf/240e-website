Title: Week 5 - Audio Feature Extraction III - Higher level features
Date: 03-02-2014

Notes:

* [Rhythm and Pitch](|filename|/notes/Rhythm_and_pitch.html)

Ipython notebooks:

* [Audio Features IIIa-Tempo](http://nbviewer.ipython.org/urls/raw2.github.com/mantaraya36/240E-ipython/master/Audio%20Features%20IIIa-Tempo.ipynb)
* [Audio Features IIIb-Pitch](http://nbviewer.ipython.org/urls/raw2.github.com/mantaraya36/240E-ipython/master/Audio%20Features%20IIIb-Pitch.ipynb?create=1)

Homework 5
----------
*due: Friday 14th February*

Use pitch or tempo extractors from an existing library (Essentia, Marsyas, librosa, etc.) to extract tempo and beat information from your collection. Verify the results, to try to identify wrong estimations and discuss the reasons for this.

Reading
-------
*due: Friday 14th February*

Klapuri, A., & Davy, M. (2006). Signal Processing Methods for Music Transcription. Part II- Chapter 4: Beat Tracking and Musical Metre Analysis. Pages 101-127.

Further Reading
---------------
Lerch, A. (n.d.). Chapter 6: Temporal analysis. In An Introduction to Audio Content Analysis (pp. 119–137).