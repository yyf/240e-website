Title: Week 2 - Analysis of symbolic representations
Date: 13-01-2014

Notes:

* [MIR with Symbolic Representations](|filename|/notes/MIR_for_Symbolic_representations.html)

Ipython notebooks:

* [Symbolic MIR with Music21](http://nbviewer.ipython.org/github/mantaraya36/240E-ipython/blob/master/Symbolic%20MIR%20Using%20Music21.ipynb)
* [Audio and Image I/O](http://nbviewer.ipython.org/urls/raw2.github.com/mantaraya36/201A-ipython/master/Image%20and%20Audio%20IO.ipynb?create=1)

Homework
--------
*due: Friday January 24th*

Perform an analysis of a set of MIDI files of your choice. Set out a hypo
thesis or goal for analysis, then perform it and discuss the results.

Submit an ipython notebook or a similar document from another environment showing the work performed, including a description of the source data set and the discussion of the results.

Reading:
--------
Cuthbert, M., Ariza, C., & Friedland, L. (2011). Feature Extraction and Machine Learning on Symbolic Music using the music21 Toolkit. In ISMIR. Retrieved from [http://web.mit.edu/music21/papers/Cuthbert_Ariza_Friedland_Feature-Extraction_ISMIR_2011.pdf](http://web.mit.edu/music21/papers/Cuthbert_Ariza_Friedland_Feature-Extraction_ISMIR_2011.pdf)

Additional Readings:
--------------------
McKay, C., & Fujinaga, I. (2006). jSymbolic: A feature extractor for MIDI files. In Proceedings of the International Computer Music Conference 2006. Retrieved from [https://www.music.mcgill.ca/~cmckay/papers/musictech/McKay_ICMC_06_jSymbolic.pdf](https://www.music.mcgill.ca/~cmckay/papers/musictech/McKay_ICMC_06_jSymbolic.pdf)

Mckay, C. (2004). Automatic Genre Classification of MIDI Recordings. PhD Thesis.

Knopke, I. (2012). Chapter 11 : Symbolic Data Mining in Musicology. In Music Data Mining (pp. 327–345).